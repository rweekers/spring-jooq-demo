create table SHIPMENTS
(
    ID        bigserial not null primary key,
    REFERENCE text      not null unique
);

create table SHIPMENT_LINES
(
    ID          bigserial not null primary key,
    QUANTITY    int       not null check (QUANTITY > 0),
    ARTICLE     text      not null,
    SHIPMENT_ID bigint    not null references SHIPMENTS (id)
);