# Setup and run

In order to run the application and tests first create the docker image and run it:

```shell
docker build -t shipments .
docker run -dp 5432:5432 shipments
```

The database should now be up and running with the appropriate tables (defined in `init.sql`) and can be connected to with `jdbc:postgresql://localhost:5432/shipments`.
 