package nl.craftsmen.springjooqdemo

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("springAutoconfiguration")
class ShipmentRepositoryAutoConfigTest {

    @Autowired
    private lateinit var shipmentRepository: ShipmentRepository

    @Autowired
    private lateinit var testShipmentRepository: TestShipmentRepository

    @BeforeEach
    fun init() {
        testShipmentRepository.deleteAll()
    }

    @Test
    fun test() {
        val corruptShipment = Shipment(
            reference = "#1",
            lines = listOf(
                ShipmentLine(
                    quantity = 0,
                    article = "PS5"
                )
            )
        )

        assertThrows<DataIntegrityViolationException> { shipmentRepository.save(corruptShipment) }
        assertTrue(shipmentRepository.getShipments().isEmpty())
        assertTrue(testShipmentRepository.findShipmentsWithOrWithoutLinesById().isEmpty())
    }
}