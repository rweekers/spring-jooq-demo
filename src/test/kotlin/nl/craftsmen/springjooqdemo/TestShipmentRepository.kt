package nl.craftsmen.springjooqdemo

import nl.craftsmen.springjooqdemo.db.generated.Tables.SHIPMENTS
import nl.craftsmen.springjooqdemo.db.generated.Tables.SHIPMENT_LINES
import nl.craftsmen.springjooqdemo.db.generated.tables.records.ShipmentsRecord
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class TestShipmentRepository @Autowired constructor(
    private val dsl: DSLContext
) {

    fun deleteAll() {
        dsl.deleteFrom(SHIPMENT_LINES).execute()
        dsl.deleteFrom(SHIPMENTS).execute()
    }

    fun findShipmentsWithOrWithoutLinesById(): List<Shipment> {
        return dsl.select()
            .from(SHIPMENTS)
            .fetch()
            .into(ShipmentsRecord::class.java)
            .map { r ->
                Shipment(
                    id = r.id,
                    reference = r.reference,
                    lines = emptyList()
                )
            }
    }
}