package nl.craftsmen.springjooqdemo

data class Shipment(
    val id: Long? = null,
    val reference: String,
    val lines: List<ShipmentLine>
)

data class ShipmentLine(
    var id: Long? = null,
    val quantity: Int,
    val article: String
)