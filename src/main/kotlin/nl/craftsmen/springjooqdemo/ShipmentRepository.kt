package nl.craftsmen.springjooqdemo

import nl.craftsmen.springjooqdemo.db.generated.Tables.SHIPMENTS
import nl.craftsmen.springjooqdemo.db.generated.Tables.SHIPMENT_LINES
import nl.craftsmen.springjooqdemo.db.generated.tables.records.ShipmentLinesRecord
import nl.craftsmen.springjooqdemo.db.generated.tables.records.ShipmentsRecord
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
class ShipmentRepository @Autowired constructor(
    private val dsl: DSLContext
) {

    fun getShipments(): List<Shipment> {
        return dsl.select()
            .from(SHIPMENTS)
            .innerJoin(SHIPMENT_LINES)
            .on(SHIPMENTS.ID.eq(SHIPMENT_LINES.SHIPMENT_ID))
            .fetch()
            .intoGroups(
                { joined -> joined.into(ShipmentsRecord::class.java) },
                { joined -> joined.into(ShipmentLinesRecord::class.java) })
            .map { Shipment(
                id = it.key.id,
                reference = it.key.reference,
                lines = it.value.map { orderLineRecord -> ShipmentLine(article = orderLineRecord.article, quantity = orderLineRecord.quantity) }) }
    }

    @Transactional
    fun save(shipment: Shipment): Shipment {
        val orderRecord = ShipmentsRecord()
        orderRecord.reference = shipment.reference
        val savedOrder = dsl.insertInto(SHIPMENTS).set(orderRecord).returning().fetchOne()

        shipment.lines
            .forEach { orderLine ->
                val orderLineRecord = ShipmentLinesRecord()
                orderLineRecord.quantity = orderLine.quantity
                orderLineRecord.article = orderLine.article
                orderLineRecord.shipmentId = savedOrder?.id
                val persisted = dsl.insertInto(SHIPMENT_LINES).set(orderLineRecord).returning().fetchOne()
                orderLine.id = persisted?.id
            }

        return shipment.copy(id = savedOrder?.id)
    }
}