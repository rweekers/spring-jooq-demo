package nl.craftsmen.springjooqdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringJooqDemoApplication

fun main(args: Array<String>) {
	runApplication<SpringJooqDemoApplication>(*args)
}
