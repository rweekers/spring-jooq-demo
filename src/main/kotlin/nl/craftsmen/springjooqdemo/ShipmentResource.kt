package nl.craftsmen.springjooqdemo

import org.jooq.exception.DataAccessException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/shipments")
class ShipmentResource {

    @Autowired
    private lateinit var shipmentRepository: ShipmentRepository

    @GetMapping
    fun shipments(): List<Shipment> =
        shipmentRepository.getShipments()

    @PostMapping
    fun saveShipment(@RequestBody newShipment: Shipment): Shipment =
        shipmentRepository.save(newShipment)

    @ExceptionHandler(value = [DataAccessException::class, DataIntegrityViolationException::class])
    fun handleException(): ResponseEntity<String> =
        ResponseEntity("Oops, something went wrong there", HttpStatus.INTERNAL_SERVER_ERROR)
}