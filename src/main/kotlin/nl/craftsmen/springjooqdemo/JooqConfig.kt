package nl.craftsmen.springjooqdemo

import org.jooq.ConnectionProvider
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DataSourceConnectionProvider
import org.jooq.impl.DefaultDSLContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy
import javax.sql.DataSource

@Configuration
@Profile("!springAutoconfiguration")
class JooqConfig {

    @Bean
    fun dslContext(dataSourceConnectionProvider: ConnectionProvider): DSLContext {
        return DefaultDSLContext(dataSourceConnectionProvider, SQLDialect.POSTGRES)
    }

    @Bean
    @Profile("!correctConfig")
    fun dataSourceConnectionProvider(dataSource: DataSource): DataSourceConnectionProvider {
        return DataSourceConnectionProvider(dataSource)
    }

    @Bean
    @Profile("correctConfig")
    fun dataSourceConnectionProviderTransactionAware(dataSource: DataSource): DataSourceConnectionProvider {
        return DataSourceConnectionProvider(TransactionAwareDataSourceProxy(dataSource))
    }
}